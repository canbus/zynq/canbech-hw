Footprint and symbol origins and licenses
-----------------------------------------

MicroZed JX Headers (modified):
https://github.com/mng2/kicad-parts
public domain

D-SUB schematic symbols and footprints:
KiCad library

TO-263-7-TEXAS:
https://github.com/cpavlina/kicad-pcblib/tree/master/manuf.pretty
CC0 1.0 Universal

SOIC-20, TSSOP20:
https://github.com/mcous/kicad-lib
CC BY-SA 3.0
