EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:microchip_dspic33dsc
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:Altera
LIBS:analog_devices
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:ir
LIBS:Lattice
LIBS:logo
LIBS:maxim
LIBS:motor_drivers
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:Xicor
LIBS:Zilog
LIBS:MCP2562FD
LIBS:vccio
LIBS:mcp
LIBS:JX1
LIBS:JX2
LIBS:gates
LIBS:usblc6
LIBS:canbench-hw-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 10
Title "CANbus MicroZed CarrierCard"
Date "25.4.2016"
Rev "RevA"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR055
U 1 1 57134BAB
P 7450 5900
F 0 "#PWR055" H 7450 5650 50  0001 C CNN
F 1 "GND" H 7450 5750 50  0000 C CNN
F 2 "" H 7450 5900 50  0000 C CNN
F 3 "" H 7450 5900 50  0000 C CNN
	1    7450 5900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR056
U 1 1 57134BB1
P 9650 6000
F 0 "#PWR056" H 9650 5750 50  0001 C CNN
F 1 "GND" H 9650 5850 50  0000 C CNN
F 2 "" H 9650 6000 50  0000 C CNN
F 3 "" H 9650 6000 50  0000 C CNN
	1    9650 6000
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR057
U 1 1 57134BB7
P 6150 3600
F 0 "#PWR057" H 6150 3450 50  0001 C CNN
F 1 "+5V" H 6150 3740 50  0000 C CNN
F 2 "" H 6150 3600 50  0000 C CNN
F 3 "" H 6150 3600 50  0000 C CNN
	1    6150 3600
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR058
U 1 1 57134BBD
P 11000 3650
F 0 "#PWR058" H 11000 3500 50  0001 C CNN
F 1 "+5V" H 11000 3790 50  0000 C CNN
F 2 "" H 11000 3650 50  0000 C CNN
F 3 "" H 11000 3650 50  0000 C CNN
	1    11000 3650
	1    0    0    -1  
$EndComp
$Comp
L VCCIO_34 #PWR059
U 1 1 57134BC3
P 6150 4600
F 0 "#PWR059" H 6150 4450 50  0001 C CNN
F 1 "VCCIO_34" H 6150 4750 50  0000 C CNN
F 2 "" H 6150 4600 50  0000 C CNN
F 3 "" H 6150 4600 50  0000 C CNN
	1    6150 4600
	1    0    0    -1  
$EndComp
$Comp
L VCCIO_34 #PWR060
U 1 1 57134BC9
P 11000 4650
F 0 "#PWR060" H 11000 4500 50  0001 C CNN
F 1 "VCCIO_34" H 11000 4800 50  0000 C CNN
F 2 "" H 11000 4650 50  0000 C CNN
F 3 "" H 11000 4650 50  0000 C CNN
	1    11000 4650
	1    0    0    -1  
$EndComp
$Comp
L PMOD PA1
U 1 1 57134C13
P 1950 1200
F 0 "PA1" H 1950 1550 50  0000 C CNN
F 1 "PMOD" H 1950 850 50  0000 C CNN
F 2 "footprints:PMOD_Angled" H 1950 0   50  0001 C CNN
F 3 "" H 1950 0   50  0000 C CNN
	1    1950 1200
	1    0    0    -1  
$EndComp
$Comp
L VCCIO_34 #PWR061
U 1 1 57134C1A
P 1500 850
F 0 "#PWR061" H 1500 700 50  0001 C CNN
F 1 "VCCIO_34" H 1500 1000 50  0000 C CNN
F 2 "" H 1500 850 50  0000 C CNN
F 3 "" H 1500 850 50  0000 C CNN
	1    1500 850 
	1    0    0    -1  
$EndComp
$Comp
L VCCIO_34 #PWR062
U 1 1 57134C22
P 2400 850
F 0 "#PWR062" H 2400 700 50  0001 C CNN
F 1 "VCCIO_34" H 2400 1000 50  0000 C CNN
F 2 "" H 2400 850 50  0000 C CNN
F 3 "" H 2400 850 50  0000 C CNN
	1    2400 850 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR063
U 1 1 57134C2A
P 2350 1550
F 0 "#PWR063" H 2350 1300 50  0001 C CNN
F 1 "GND" H 2350 1400 50  0000 C CNN
F 2 "" H 2350 1550 50  0000 C CNN
F 3 "" H 2350 1550 50  0000 C CNN
	1    2350 1550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR064
U 1 1 57134C32
P 1550 1550
F 0 "#PWR064" H 1550 1300 50  0001 C CNN
F 1 "GND" H 1550 1400 50  0000 C CNN
F 2 "" H 1550 1550 50  0000 C CNN
F 3 "" H 1550 1550 50  0000 C CNN
	1    1550 1550
	1    0    0    -1  
$EndComp
Text Label 1050 950  0    60   ~ 0
PA1_2_P
Text Label 1050 1050 0    60   ~ 0
PA1_2_N
Text Label 1050 1150 0    60   ~ 0
PA3_4_P
Text Label 1050 1250 0    60   ~ 0
PA3_4_N
Text Label 2900 950  2    60   ~ 0
PA7_8_P
Text Label 2900 1050 2    60   ~ 0
PA7_8_N
Text Label 2900 1150 2    60   ~ 0
PA9_10_P
Text Label 2900 1250 2    60   ~ 0
PA9_10_N
$Comp
L PMOD PB1
U 1 1 57134C49
P 1950 2500
F 0 "PB1" H 1950 2850 50  0000 C CNN
F 1 "PMOD" H 1950 2150 50  0000 C CNN
F 2 "footprints:PMOD_Angled" H 1950 1300 50  0001 C CNN
F 3 "" H 1950 1300 50  0000 C CNN
	1    1950 2500
	1    0    0    -1  
$EndComp
$Comp
L VCCIO_34 #PWR065
U 1 1 57134C50
P 1500 2150
F 0 "#PWR065" H 1500 2000 50  0001 C CNN
F 1 "VCCIO_34" H 1500 2300 50  0000 C CNN
F 2 "" H 1500 2150 50  0000 C CNN
F 3 "" H 1500 2150 50  0000 C CNN
	1    1500 2150
	1    0    0    -1  
$EndComp
$Comp
L VCCIO_34 #PWR066
U 1 1 57134C58
P 2400 2150
F 0 "#PWR066" H 2400 2000 50  0001 C CNN
F 1 "VCCIO_34" H 2400 2300 50  0000 C CNN
F 2 "" H 2400 2150 50  0000 C CNN
F 3 "" H 2400 2150 50  0000 C CNN
	1    2400 2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR067
U 1 1 57134C60
P 2350 2850
F 0 "#PWR067" H 2350 2600 50  0001 C CNN
F 1 "GND" H 2350 2700 50  0000 C CNN
F 2 "" H 2350 2850 50  0000 C CNN
F 3 "" H 2350 2850 50  0000 C CNN
	1    2350 2850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR068
U 1 1 57134C66
P 1550 2850
F 0 "#PWR068" H 1550 2600 50  0001 C CNN
F 1 "GND" H 1550 2700 50  0000 C CNN
F 2 "" H 1550 2850 50  0000 C CNN
F 3 "" H 1550 2850 50  0000 C CNN
	1    1550 2850
	1    0    0    -1  
$EndComp
Text Label 1050 2250 0    60   ~ 0
PB1_2_P
Text Label 1050 2350 0    60   ~ 0
PB1_2_N
Text Label 1050 2450 0    60   ~ 0
PB3_4_P
Text Label 1050 2550 0    60   ~ 0
PB3_4_N
Text Label 2900 2250 2    60   ~ 0
PB7_8_P
Text Label 2900 2350 2    60   ~ 0
PB7_8_N
Text Label 2900 2450 2    60   ~ 0
PB9_10_P
Text Label 2900 2550 2    60   ~ 0
PB9_10_N
$Comp
L +5V #PWR069
U 1 1 57134C76
P 3200 4050
F 0 "#PWR069" H 3200 3900 50  0001 C CNN
F 1 "+5V" H 3200 4190 50  0000 C CNN
F 2 "" H 3200 4050 50  0000 C CNN
F 3 "" H 3200 4050 50  0000 C CNN
	1    3200 4050
	1    0    0    -1  
$EndComp
$Comp
L VCCIO_34 #PWR070
U 1 1 57134C81
P 1450 4050
F 0 "#PWR070" H 1450 3900 50  0001 C CNN
F 1 "VCCIO_34" H 1450 4200 50  0000 C CNN
F 2 "" H 1450 4050 50  0000 C CNN
F 3 "" H 1450 4050 50  0000 C CNN
	1    1450 4050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR071
U 1 1 57134C89
P 3150 6250
F 0 "#PWR071" H 3150 6000 50  0001 C CNN
F 1 "GND" H 3150 6100 50  0000 C CNN
F 2 "" H 3150 6250 50  0000 C CNN
F 3 "" H 3150 6250 50  0000 C CNN
	1    3150 6250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR072
U 1 1 57134C95
P 1550 6250
F 0 "#PWR072" H 1550 6000 50  0001 C CNN
F 1 "GND" H 1550 6100 50  0000 C CNN
F 2 "" H 1550 6250 50  0000 C CNN
F 3 "" H 1550 6250 50  0000 C CNN
	1    1550 6250
	1    0    0    -1  
$EndComp
$Comp
L RPI_GPIO_HEADER_02X20 PP1
U 1 1 57134CC1
P 2350 5100
F 0 "PP1" H 2000 6150 50  0000 C CNN
F 1 "RPI_GPIO_HEADER_02X20" H 2400 4000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x20" H 2000 4150 50  0001 C CNN
F 3 "" H 2000 4150 50  0000 C CNN
	1    2350 5100
	1    0    0    -1  
$EndComp
Text Label 6400 1650 0    60   ~ 0
PB1_2_P
Text Label 6400 1750 0    60   ~ 0
PB1_2_N
Text Label 6400 1950 0    60   ~ 0
PB3_4_P
Text Label 6400 1350 0    60   ~ 0
PB7_8_P
Text Label 6400 2250 0    60   ~ 0
PB9_10_P
Text Label 6400 2050 0    60   ~ 0
PB3_4_N
Text Label 6400 1450 0    60   ~ 0
PB7_8_N
Text Label 6400 2350 0    60   ~ 0
PB9_10_N
Text Label 10700 4950 2    60   ~ 0
PX1
Text Label 10700 4850 2    60   ~ 0
PX2
Text Label 6400 4950 0    60   ~ 0
PX3
Text Label 6400 4850 0    60   ~ 0
PX4
Text Label 6400 2850 0    60   ~ 0
PA1_2_P
Text Label 6400 2950 0    60   ~ 0
PA1_2_N
Text Label 6400 3150 0    60   ~ 0
PA3_4_P
Text Label 6400 3250 0    60   ~ 0
PA3_4_N
Text Label 6400 2550 0    60   ~ 0
PA7_8_P
Text Label 6400 2650 0    60   ~ 0
PA7_8_N
Text Label 6400 3450 0    60   ~ 0
PA9_10_P
Text Label 6400 3550 0    60   ~ 0
PA9_10_N
$Comp
L VCCIO_34 #PWR073
U 1 1 57134D14
P 4300 850
F 0 "#PWR073" H 4300 700 50  0001 C CNN
F 1 "VCCIO_34" H 4200 950 50  0000 C CNN
F 2 "" H 4300 850 50  0000 C CNN
F 3 "" H 4300 850 50  0000 C CNN
	1    4300 850 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR074
U 1 1 57134D1C
P 4450 1700
F 0 "#PWR074" H 4450 1450 50  0001 C CNN
F 1 "GND" H 4450 1550 50  0000 C CNN
F 2 "" H 4450 1700 50  0000 C CNN
F 3 "" H 4450 1700 50  0000 C CNN
	1    4450 1700
	1    0    0    -1  
$EndComp
Text Label 650  4250 0    60   ~ 0
PP_GPIO2_SDA
Text Label 650  4350 0    60   ~ 0
PP_GPIO3_SCL
Text Label 650  4450 0    60   ~ 0
PP_GPIO4
Text Label 650  4650 0    60   ~ 0
PP_GPIO17
Text Label 650  4750 0    60   ~ 0
PP_GPIO27
Text Label 650  4850 0    60   ~ 0
PP_GPIO22
Text Label 650  5050 0    60   ~ 0
PP_GPIO10_MOSI
Text Label 650  5150 0    60   ~ 0
PP_GPIO9_MISO
Text Label 650  5250 0    60   ~ 0
PP_GPIO11_SCLK
Text Label 650  5450 0    60   ~ 0
PP_ID_SD
Text Label 650  5550 0    60   ~ 0
PP_GPIO5
Text Label 650  5650 0    60   ~ 0
PP_GPIO6
Text Label 650  5750 0    60   ~ 0
PP_GPIO13
Text Label 650  5850 0    60   ~ 0
PP_GPIO19
Text Label 650  5950 0    60   ~ 0
PP_GPIO26
Text Label 3950 4450 2    60   ~ 0
PP_GPIO14_TXD
Text Label 3950 4550 2    60   ~ 0
PP_GPIO15_RXD
Text Label 3950 4650 2    60   ~ 0
PP_GPIO18_PWM
Text Label 3950 4850 2    60   ~ 0
PP_GPIO23
Text Label 3950 4950 2    60   ~ 0
PP_GPIO24
Text Label 3950 5150 2    60   ~ 0
PP_GPIO25
Text Label 3950 5250 2    60   ~ 0
PP_GPIO8_CE0
Text Label 3950 5350 2    60   ~ 0
PP_GPIO7_CE1
Text Label 3950 5450 2    60   ~ 0
PP_ID_SC
Text Label 3950 5650 2    60   ~ 0
PP_GPIO12
Text Label 3950 5850 2    60   ~ 0
PP_GPIO16
Text Label 3950 5950 2    60   ~ 0
PP_GPIO20
Text Label 3950 6050 2    60   ~ 0
PP_GPIO21
Text Label 10600 4550 2    60   ~ 0
PP_GPIO2_SDA
Text Label 10600 4450 2    60   ~ 0
PP_GPIO3_SCL
Text Label 6400 4250 0    60   ~ 0
PP_GPIO17
Text Label 6400 4150 0    60   ~ 0
PP_GPIO27
Text Label 10600 3850 2    60   ~ 0
PP_GPIO22
Text Label 6400 3850 0    60   ~ 0
PP_GPIO10_MOSI
Text Label 10600 3450 2    60   ~ 0
PP_GPIO9_MISO
Text Label 10600 3150 2    60   ~ 0
PP_GPIO11_SCLK
Text Label 10600 2650 2    60   ~ 0
PP_ID_SD
Text Label 10600 2550 2    60   ~ 0
PP_GPIO5
Text Label 10600 2250 2    60   ~ 0
PP_GPIO6
Text Label 10600 2050 2    60   ~ 0
PP_GPIO13
Text Label 10600 1750 2    60   ~ 0
PP_GPIO19
Text Label 10600 1450 2    60   ~ 0
PP_GPIO26
Text Label 6400 4550 0    60   ~ 0
PP_GPIO14_TXD
Text Label 10600 4250 2    60   ~ 0
PP_GPIO15_RXD
Text Label 10600 4150 2    60   ~ 0
PP_GPIO18_PWM
Text Label 10600 3950 2    60   ~ 0
PP_GPIO23
Text Label 6400 3950 0    60   ~ 0
PP_GPIO24
Text Label 10600 3550 2    60   ~ 0
PP_GPIO25
Text Label 10600 3250 2    60   ~ 0
PP_GPIO8_CE0
Text Label 10600 2950 2    60   ~ 0
PP_GPIO7_CE1
Text Label 10600 2850 2    60   ~ 0
PP_ID_SC
Text Label 10600 2350 2    60   ~ 0
PP_GPIO12
Text Label 10600 1950 2    60   ~ 0
PP_GPIO16
Text Label 10600 1650 2    60   ~ 0
PP_GPIO20
Text Label 10600 1350 2    60   ~ 0
PP_GPIO21
Text Label 6400 4450 0    60   ~ 0
PP_GPIO4
NoConn ~ 7500 5450
NoConn ~ 7500 5650
NoConn ~ 7500 5750
NoConn ~ 9500 5450
NoConn ~ 9500 5650
NoConn ~ 9500 5750
Text Label 6650 1050 0    60   ~ 0
CARRIER_SRST#
Text Label 10400 1050 2    60   ~ 0
PWR_ENABLE
Text Label 6650 1150 0    60   ~ 0
FPGA_DONE
Text HLabel 10400 1050 2    60   3State ~ 0
PWR_ENABLE
$Comp
L C C23
U 1 1 57134D75
P 3900 6950
F 0 "C23" H 3925 7050 50  0000 L CNN
F 1 "10n" H 3925 6850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3938 6800 50  0001 C CNN
F 3 "" H 3900 6950 50  0000 C CNN
	1    3900 6950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR075
U 1 1 57134D7C
P 3200 7200
F 0 "#PWR075" H 3200 6950 50  0001 C CNN
F 1 "GND" H 3200 7050 50  0000 C CNN
F 2 "" H 3200 7200 50  0000 C CNN
F 3 "" H 3200 7200 50  0000 C CNN
	1    3200 7200
	1    0    0    -1  
$EndComp
Text Label 3800 6700 0    50   ~ 0
CARRIER_SRST#
Text Notes 3250 6500 0    60   ~ 0
Reset Ctrl
$Comp
L GND #PWR076
U 1 1 57134D84
P 5850 7500
F 0 "#PWR076" H 5850 7250 50  0001 C CNN
F 1 "GND" H 5850 7350 50  0000 C CNN
F 2 "" H 5850 7500 50  0000 C CNN
F 3 "" H 5850 7500 50  0000 C CNN
	1    5850 7500
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 57134D8A
P 5250 6800
F 0 "R8" V 5330 6800 50  0000 C CNN
F 1 "270R" V 5250 6800 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5180 6800 50  0001 C CNN
F 3 "" H 5250 6800 50  0000 C CNN
	1    5250 6800
	0    1    1    0   
$EndComp
Text Label 4900 7200 0    60   ~ 0
FPGA_DONE
$Comp
L BSS138 Q2
U 1 1 57134D92
P 5750 7150
F 0 "Q2" H 5950 7225 50  0000 L CNN
F 1 "BSS138" H 5950 7150 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 5950 7075 50  0001 L CIN
F 3 "" H 5750 7150 50  0000 L CNN
	1    5750 7150
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR077
U 1 1 57134D99
P 5000 6650
F 0 "#PWR077" H 5000 6500 50  0001 C CNN
F 1 "+5V" H 5000 6790 50  0000 C CNN
F 2 "" H 5000 6650 50  0000 C CNN
F 3 "" H 5000 6650 50  0000 C CNN
	1    5000 6650
	1    0    0    -1  
$EndComp
$Comp
L LED D6
U 1 1 57134D9F
P 5650 6800
F 0 "D6" H 5650 6900 50  0000 C CNN
F 1 "LED_BLUE" H 5650 6700 50  0000 C CNN
F 2 "LEDs:LED_0805" H 5650 6800 50  0001 C CNN
F 3 "" H 5650 6800 50  0000 C CNN
	1    5650 6800
	-1   0    0    1   
$EndComp
Text Notes 5400 6500 0    60   ~ 0
DONE LED
$Comp
L SW_PUSH SW3
U 1 1 57134DB8
P 3500 6700
F 0 "SW3" H 3650 6810 50  0000 C CNN
F 1 "SW_PUSH" H 3500 6620 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_EVQP0" H 3500 6700 50  0001 C CNN
F 3 "" H 3500 6700 50  0000 C CNN
	1    3500 6700
	1    0    0    -1  
$EndComp
$Comp
L MicroZed_JX1 JX1
U 1 1 57134DBF
P 8500 3300
F 0 "JX1" H 8500 5900 50  0000 C CNN
F 1 "MicroZed_JX1" H 8500 700 50  0000 C CNN
F 2 "footprints:61083_10x" H 8450 3250 60  0001 C CNN
F 3 "" H 8500 3300 60  0000 C CNN
	1    8500 3300
	-1   0    0    -1  
$EndComp
NoConn ~ 9500 5250
NoConn ~ 9500 5150
NoConn ~ 7500 5350
NoConn ~ 7500 5250
NoConn ~ 7500 5150
NoConn ~ 9500 850 
NoConn ~ 9500 950 
NoConn ~ 9500 1150
NoConn ~ 9500 1250
NoConn ~ 7500 950 
NoConn ~ 7500 850 
NoConn ~ 9500 5350
NoConn ~ 7500 1250
$Comp
L CONN_01X08 PX1
U 1 1 57182E22
P 4700 1300
F 0 "PX1" H 4700 1750 50  0000 C CNN
F 1 "CONN_01X08" V 4800 1300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08" H 4700 1300 50  0001 C CNN
F 3 "" H 4700 1300 50  0000 C CNN
	1    4700 1300
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR078
U 1 1 5718351C
P 4400 700
F 0 "#PWR078" H 4400 550 50  0001 C CNN
F 1 "+5V" H 4400 840 50  0000 C CNN
F 2 "" H 4400 700 50  0000 C CNN
F 3 "" H 4400 700 50  0000 C CNN
	1    4400 700 
	1    0    0    -1  
$EndComp
$Comp
L R R55
U 1 1 57183B7E
P 4100 950
F 0 "R55" V 4050 750 50  0000 C CNN
F 1 "100R" V 4100 950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4030 950 50  0001 C CNN
F 3 "" H 4100 950 50  0000 C CNN
	1    4100 950 
	0    1    1    0   
$EndComp
$Comp
L R R56
U 1 1 57183D46
P 4100 1050
F 0 "R56" V 4050 850 50  0000 C CNN
F 1 "100R" V 4100 1050 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4030 1050 50  0001 C CNN
F 3 "" H 4100 1050 50  0000 C CNN
	1    4100 1050
	0    1    1    0   
$EndComp
$Comp
L R R57
U 1 1 57183E5A
P 4100 1150
F 0 "R57" V 4050 950 50  0000 C CNN
F 1 "100R" V 4100 1150 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4030 1150 50  0001 C CNN
F 3 "" H 4100 1150 50  0000 C CNN
	1    4100 1150
	0    1    1    0   
$EndComp
$Comp
L R R58
U 1 1 57183E91
P 4100 1250
F 0 "R58" V 4050 1050 50  0000 C CNN
F 1 "100R" V 4100 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4030 1250 50  0001 C CNN
F 3 "" H 4100 1250 50  0000 C CNN
	1    4100 1250
	0    1    1    0   
$EndComp
Text Label 3400 950  0    60   ~ 0
CON_PX1
Text Label 3400 1050 0    60   ~ 0
CON_PX2
Text Label 3400 1150 0    60   ~ 0
CON_PX3
Text Label 3400 1250 0    60   ~ 0
CON_PX4
Wire Wire Line
	7500 1550 7450 1550
Wire Wire Line
	7450 1550 7450 1850
Wire Wire Line
	7450 1850 7450 2150
Wire Wire Line
	7450 2150 7450 2450
Wire Wire Line
	7450 2450 7450 2750
Wire Wire Line
	7450 2750 7450 3050
Wire Wire Line
	7450 3050 7450 3350
Wire Wire Line
	7450 3350 7450 4050
Wire Wire Line
	7450 4050 7450 4350
Wire Wire Line
	7450 4350 7450 5050
Wire Wire Line
	7450 5050 7450 5550
Wire Wire Line
	7450 5550 7450 5900
Wire Wire Line
	7500 5050 7450 5050
Connection ~ 7450 5050
Wire Wire Line
	7500 4350 7450 4350
Connection ~ 7450 4350
Wire Wire Line
	7500 4050 7450 4050
Connection ~ 7450 4050
Wire Wire Line
	7500 3350 7450 3350
Connection ~ 7450 3350
Wire Wire Line
	6400 4450 7500 4450
Wire Wire Line
	7450 3050 7500 3050
Connection ~ 7450 3050
Wire Wire Line
	7500 2750 7450 2750
Connection ~ 7450 2750
Wire Wire Line
	7500 2450 7450 2450
Connection ~ 7450 2450
Wire Wire Line
	7500 2150 7450 2150
Connection ~ 7450 2150
Wire Wire Line
	7500 1850 7450 1850
Connection ~ 7450 1850
Wire Wire Line
	9500 1550 9650 1550
Wire Wire Line
	9650 1550 9650 1850
Wire Wire Line
	9650 1850 9650 2150
Wire Wire Line
	9650 2150 9650 2450
Wire Wire Line
	9650 2450 9650 2750
Wire Wire Line
	9650 2750 9650 3050
Wire Wire Line
	9650 3050 9650 3350
Wire Wire Line
	9650 3350 9650 4050
Wire Wire Line
	9650 4050 9650 4350
Wire Wire Line
	9650 4350 9650 4650
Wire Wire Line
	9650 4650 9650 5050
Wire Wire Line
	9650 5050 9650 5550
Wire Wire Line
	9650 5550 9650 6000
Wire Wire Line
	9500 5050 9650 5050
Connection ~ 9650 5050
Wire Wire Line
	9500 4650 9650 4650
Connection ~ 9650 4650
Wire Wire Line
	9500 4350 9650 4350
Connection ~ 9650 4350
Wire Wire Line
	9500 4050 9650 4050
Connection ~ 9650 4050
Wire Wire Line
	9500 3350 9650 3350
Connection ~ 9650 3350
Wire Wire Line
	9500 3050 9650 3050
Connection ~ 9650 3050
Wire Wire Line
	9500 2750 9650 2750
Connection ~ 9650 2750
Wire Wire Line
	9500 2450 9650 2450
Connection ~ 9650 2450
Wire Wire Line
	9500 2150 9650 2150
Connection ~ 9650 2150
Wire Wire Line
	9500 1850 9650 1850
Connection ~ 9650 1850
Wire Wire Line
	6150 3750 7350 3750
Wire Wire Line
	7350 3750 7500 3750
Wire Wire Line
	6150 3750 6150 3600
Wire Wire Line
	7350 3650 7500 3650
Wire Wire Line
	9500 3750 9750 3750
Wire Wire Line
	9750 3750 11000 3750
Wire Wire Line
	11000 3750 11000 3650
Wire Wire Line
	9500 3650 9750 3650
Wire Wire Line
	9750 3650 9750 3750
Connection ~ 9750 3750
Wire Wire Line
	6150 4750 7350 4750
Wire Wire Line
	7350 4750 7500 4750
Wire Wire Line
	6150 4750 6150 4600
Wire Wire Line
	7500 4650 7350 4650
Wire Wire Line
	7350 4650 7350 4750
Connection ~ 7350 4750
Wire Wire Line
	9500 4750 11000 4750
Wire Wire Line
	11000 4750 11000 4650
Wire Wire Line
	2300 950  2900 950 
Wire Wire Line
	2300 1050 2900 1050
Wire Wire Line
	2300 1150 2900 1150
Wire Wire Line
	2300 1250 2900 1250
Wire Wire Line
	1050 950  1600 950 
Wire Wire Line
	1050 1050 1600 1050
Wire Wire Line
	1050 1150 1600 1150
Wire Wire Line
	1050 1250 1600 1250
Wire Wire Line
	1600 1450 1500 1450
Wire Wire Line
	1500 1450 1500 850 
Wire Wire Line
	2400 850  2400 1450
Wire Wire Line
	2400 1450 2300 1450
Wire Wire Line
	2300 1350 2350 1350
Wire Wire Line
	2350 1350 2350 1550
Wire Wire Line
	1550 1350 1550 1550
Wire Wire Line
	2300 2250 2900 2250
Wire Wire Line
	2300 2350 2900 2350
Wire Wire Line
	2300 2450 2900 2450
Wire Wire Line
	2300 2550 2900 2550
Wire Wire Line
	1050 2250 1600 2250
Wire Wire Line
	1050 2350 1600 2350
Wire Wire Line
	1050 2450 1600 2450
Wire Wire Line
	1050 2550 1600 2550
Wire Wire Line
	1600 2750 1500 2750
Wire Wire Line
	1500 2750 1500 2150
Wire Wire Line
	2400 2150 2400 2750
Wire Wire Line
	2400 2750 2300 2750
Wire Wire Line
	1550 2850 1550 2650
Wire Wire Line
	1550 2650 1600 2650
Wire Wire Line
	3200 4250 3200 4050
Wire Wire Line
	3000 4250 3100 4250
Wire Wire Line
	3100 4250 3200 4250
Wire Wire Line
	3000 4150 3100 4150
Wire Wire Line
	3100 4150 3100 4250
Connection ~ 3100 4250
Wire Wire Line
	1450 4050 1450 4150
Wire Wire Line
	1450 4150 1450 4950
Wire Wire Line
	1450 4150 1750 4150
Wire Wire Line
	3000 4350 3150 4350
Wire Wire Line
	3150 4350 3150 4750
Wire Wire Line
	3150 4750 3150 5050
Wire Wire Line
	3150 5050 3150 5550
Wire Wire Line
	3150 5550 3150 5750
Wire Wire Line
	3150 5750 3150 6250
Wire Wire Line
	3000 4750 3150 4750
Wire Wire Line
	3000 5050 3150 5050
Wire Wire Line
	3000 5550 3150 5550
Wire Wire Line
	3000 5750 3150 5750
Wire Wire Line
	1750 4550 1550 4550
Wire Wire Line
	1550 4550 1550 5350
Wire Wire Line
	1550 5350 1550 6050
Wire Wire Line
	1550 6050 1550 6250
Wire Wire Line
	1750 5350 1550 5350
Connection ~ 1550 5350
Wire Wire Line
	1750 6050 1550 6050
Connection ~ 1550 6050
Wire Wire Line
	1450 4950 1750 4950
Connection ~ 1450 4150
Wire Wire Line
	3000 4450 3950 4450
Wire Wire Line
	3950 4550 3000 4550
Wire Wire Line
	3950 4650 3000 4650
Wire Wire Line
	3950 4850 3000 4850
Wire Wire Line
	3950 4950 3000 4950
Wire Wire Line
	3950 5150 3000 5150
Wire Wire Line
	3950 5250 3000 5250
Wire Wire Line
	3950 5350 3000 5350
Wire Wire Line
	3950 5450 3000 5450
Wire Wire Line
	3950 5650 3000 5650
Wire Wire Line
	3950 5850 3000 5850
Wire Wire Line
	3950 5950 3000 5950
Wire Wire Line
	3950 6050 3000 6050
Wire Wire Line
	650  4250 1750 4250
Wire Wire Line
	650  4350 1750 4350
Wire Wire Line
	650  4450 1750 4450
Wire Wire Line
	650  4650 1750 4650
Wire Wire Line
	650  4750 1750 4750
Wire Wire Line
	650  4850 1750 4850
Wire Wire Line
	650  5050 1750 5050
Wire Wire Line
	650  5150 1750 5150
Wire Wire Line
	650  5250 1750 5250
Wire Wire Line
	650  5450 1750 5450
Wire Wire Line
	650  5550 1750 5550
Wire Wire Line
	650  5650 1750 5650
Wire Wire Line
	650  5750 1750 5750
Wire Wire Line
	650  5850 1750 5850
Wire Wire Line
	2300 2650 2350 2650
Wire Wire Line
	2350 2650 2350 2850
Wire Wire Line
	1550 1350 1600 1350
Wire Wire Line
	7500 1650 6400 1650
Wire Wire Line
	7500 1750 6400 1750
Wire Wire Line
	7500 1950 6400 1950
Wire Wire Line
	7500 2050 6400 2050
Wire Wire Line
	7500 1350 6400 1350
Wire Wire Line
	7500 1450 6400 1450
Wire Wire Line
	7500 2250 6400 2250
Wire Wire Line
	7500 2350 6400 2350
Wire Wire Line
	9500 4550 10600 4550
Wire Wire Line
	7500 4850 6400 4850
Wire Wire Line
	7500 4250 6400 4250
Wire Wire Line
	7500 4150 6400 4150
Wire Wire Line
	9500 3850 10600 3850
Wire Wire Line
	7500 3850 6400 3850
Wire Wire Line
	9500 3450 10600 3450
Wire Wire Line
	9500 3150 10600 3150
Wire Wire Line
	9500 2650 10600 2650
Wire Wire Line
	9500 2550 10600 2550
Wire Wire Line
	9500 2250 10600 2250
Wire Wire Line
	9500 2050 10600 2050
Wire Wire Line
	9500 1750 10600 1750
Wire Wire Line
	9500 1450 10600 1450
Wire Wire Line
	9500 1350 10600 1350
Wire Wire Line
	7350 3650 7350 3750
Connection ~ 7350 3750
Wire Wire Line
	9500 4950 10700 4950
Wire Wire Line
	9500 4850 10700 4850
Wire Wire Line
	7500 4950 6400 4950
Wire Wire Line
	9500 4450 10600 4450
Wire Wire Line
	7500 2850 6400 2850
Wire Wire Line
	7500 2950 6400 2950
Wire Wire Line
	7500 3150 6400 3150
Wire Wire Line
	7500 3250 6400 3250
Wire Wire Line
	7500 2550 6400 2550
Wire Wire Line
	7500 2650 6400 2650
Wire Wire Line
	7500 3450 6400 3450
Wire Wire Line
	7500 3550 6400 3550
Wire Wire Line
	7500 4550 6400 4550
Wire Wire Line
	9500 4250 10600 4250
Wire Wire Line
	9500 4150 10600 4150
Wire Wire Line
	9500 3950 10600 3950
Wire Wire Line
	7500 3950 6400 3950
Wire Wire Line
	9500 3550 10600 3550
Wire Wire Line
	9500 3250 10600 3250
Wire Wire Line
	9500 2950 10600 2950
Wire Wire Line
	9500 2850 10600 2850
Wire Wire Line
	9500 2350 10600 2350
Wire Wire Line
	10600 1950 9500 1950
Wire Wire Line
	9500 1650 10600 1650
Wire Wire Line
	4300 1450 4500 1450
Wire Wire Line
	4300 1450 4300 850 
Wire Wire Line
	1750 5950 650  5950
Connection ~ 3150 4750
Connection ~ 3150 5050
Connection ~ 3150 5550
Connection ~ 3150 5750
Wire Wire Line
	6650 1050 7500 1050
Wire Wire Line
	10400 1050 9500 1050
Wire Wire Line
	6650 1150 7500 1150
Wire Wire Line
	3200 6700 3200 7150
Wire Wire Line
	3200 7150 3200 7200
Wire Wire Line
	3800 6700 3900 6700
Wire Wire Line
	3900 6700 4400 6700
Wire Wire Line
	3900 6800 3900 6700
Connection ~ 3900 6700
Wire Wire Line
	3900 7150 3200 7150
Connection ~ 3200 7150
Wire Wire Line
	3900 7150 3900 7100
Wire Wire Line
	5850 7350 5850 7500
Wire Wire Line
	5000 6800 5000 6650
Wire Wire Line
	5100 6800 5000 6800
Wire Wire Line
	5400 6800 5450 6800
Wire Wire Line
	5850 6800 5850 6950
Wire Wire Line
	4900 7200 5550 7200
Wire Wire Line
	7500 5550 7450 5550
Connection ~ 7450 5550
Wire Wire Line
	9500 5550 9650 5550
Connection ~ 9650 5550
Wire Wire Line
	4500 1550 4450 1550
Wire Wire Line
	4450 1350 4450 1550
Wire Wire Line
	4450 1550 4450 1700
Wire Wire Line
	4500 1650 4400 1650
Wire Wire Line
	4400 1650 4400 700 
Wire Wire Line
	4500 1350 4450 1350
Connection ~ 4450 1550
Wire Wire Line
	4500 950  4250 950 
Wire Wire Line
	4500 1050 4250 1050
Wire Wire Line
	4500 1150 4250 1150
Wire Wire Line
	4500 1250 4250 1250
Wire Wire Line
	3400 950  3950 950 
Wire Wire Line
	3400 1050 3950 1050
Wire Wire Line
	3400 1150 3950 1150
Wire Wire Line
	3400 1250 3950 1250
Text Label 4950 2400 2    60   ~ 0
PX2
Text Label 4950 2800 2    60   ~ 0
PX1
$Comp
L USBLC6-2 D15
U 1 1 571867B7
P 4150 2600
F 0 "D15" H 4000 2950 60  0000 C CNN
F 1 "USBLC6-2SC6" H 4200 2250 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-6" H 4200 2700 60  0001 C CNN
F 3 "" H 4200 2700 60  0000 C CNN
	1    4150 2600
	1    0    0    1   
$EndComp
Wire Wire Line
	4650 2400 4950 2400
Wire Wire Line
	4650 2800 4950 2800
Wire Wire Line
	3750 2400 3200 2400
Wire Wire Line
	3750 2800 3200 2800
Text Label 3200 2400 0    60   ~ 0
CON_PX2
Text Label 3200 2800 0    60   ~ 0
CON_PX1
Text Label 4950 3200 2    60   ~ 0
PX4
Text Label 4950 3600 2    60   ~ 0
PX3
$Comp
L USBLC6-2 D16
U 1 1 571867C6
P 4150 3400
F 0 "D16" H 4000 3750 60  0000 C CNN
F 1 "USBLC6-2SC6" H 4200 3050 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-6" H 4200 3500 60  0001 C CNN
F 3 "" H 4200 3500 60  0000 C CNN
	1    4150 3400
	1    0    0    1   
$EndComp
Wire Wire Line
	4650 3200 4950 3200
Wire Wire Line
	4650 3600 4950 3600
Wire Wire Line
	3750 3200 3200 3200
Wire Wire Line
	3750 3600 3200 3600
Text Label 3200 3200 0    60   ~ 0
CON_PX4
Text Label 3200 3600 0    60   ~ 0
CON_PX3
$Comp
L VCCIO_34 #PWR079
U 1 1 571867D3
P 4700 2250
F 0 "#PWR079" H 4700 2100 50  0001 C CNN
F 1 "VCCIO_34" H 4700 2350 50  0000 C CNN
F 2 "" H 4700 2250 50  0000 C CNN
F 3 "" H 4700 2250 50  0000 C CNN
	1    4700 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 2250 4700 2600
Wire Wire Line
	4700 2600 4700 3400
Wire Wire Line
	4700 2600 4650 2600
Wire Wire Line
	4700 3400 4650 3400
Connection ~ 4700 2600
$Comp
L GND #PWR080
U 1 1 571867DD
P 3700 3750
F 0 "#PWR080" H 3700 3500 50  0001 C CNN
F 1 "GND" H 3700 3600 50  0000 C CNN
F 2 "" H 3700 3750 50  0000 C CNN
F 3 "" H 3700 3750 50  0000 C CNN
	1    3700 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 2600 3700 3400
Wire Wire Line
	3700 3400 3700 3750
Wire Wire Line
	3700 3400 3750 3400
Wire Wire Line
	3700 2600 3750 2600
Connection ~ 3700 3400
$EndSCHEMATC
