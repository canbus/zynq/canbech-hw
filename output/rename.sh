#!/bin/bash

cp canbench-hw-F.Cu.gbr      top.gbr
cp canbench-hw-F.Mask.gbr    smt.gbr
cp canbench-hw-F.SilkS.gbr   plt.gbr

cp canbench-hw-B.Cu.gbr      bot.gbr
cp canbench-hw-B.Mask.gbr    smb.gbr
cp canbench-hw-B.SilkS.gbr   plb.gbr

cp canbench-hw-Edge.Cuts.gbr mill.gbr
cp canbench-hw.drl           pth.exc
cp canbench-hw-NPTH.drl      npth.exc

#cp canbench-hw-drl_map.gbr
#cp canbench-hw-drl.rpt

rm -f can-bench.zip
# without plb and npth
zip can-bench.zip {top,smt,plt,bot,smb,mill}.gbr pth.exc
rm {top,smt,plt,bot,smb,plb,mill}.gbr {pth,npth}.exc

rm -R unpacked
unzip can-bench.zip -d unpacked
