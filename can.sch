EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:microchip_dspic33dsc
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:Altera
LIBS:analog_devices
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:ir
LIBS:Lattice
LIBS:logo
LIBS:maxim
LIBS:motor_drivers
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:Xicor
LIBS:Zilog
LIBS:MCP2562FD
LIBS:vccio
LIBS:mcp
LIBS:JX1
LIBS:JX2
LIBS:gates
LIBS:usblc6
LIBS:canbench-hw-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 10
Title "CANbus MicroZed CarrierCard"
Date "25.4.2016"
Rev "RevA"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1950 650  950  500 
U 56F326CC
F0 "CAN 1" 60
F1 "can-interface.sch" 60
F2 "RXD" O L 1950 750 60 
F3 "TXD" I L 1950 850 60 
F4 "STBY" I L 1950 950 60 
F5 "COM_CAN-" B R 2900 950 60 
F6 "COM_CAN+" B R 2900 1050 60 
$EndSheet
Text HLabel 1200 3850 0    60   Input ~ 0
CAN_STBY
Text HLabel 1400 750  0    60   Output ~ 0
CAN1_RXD
Text HLabel 1400 850  0    60   Input ~ 0
CAN1_TXD
Text HLabel 1400 1550 0    60   Output ~ 0
CAN2_RXD
Text HLabel 1400 1650 0    60   Input ~ 0
CAN2_TXD
Text HLabel 1400 2350 0    60   Output ~ 0
CAN3_RXD
Text HLabel 1400 2450 0    60   Input ~ 0
CAN3_TXD
Text HLabel 1400 3150 0    60   Output ~ 0
CAN4_RXD
Text HLabel 1400 3250 0    60   Input ~ 0
CAN4_TXD
$Sheet
S 1950 1450 950  500 
U 570971DE
F0 "CAN 2" 60
F1 "can-interface.sch" 60
F2 "RXD" O L 1950 1550 60 
F3 "TXD" I L 1950 1650 60 
F4 "STBY" I L 1950 1750 60 
F5 "COM_CAN-" B R 2900 1750 60 
F6 "COM_CAN+" B R 2900 1850 60 
$EndSheet
$Sheet
S 1950 2250 950  500 
U 57098102
F0 "CAN 3" 60
F1 "can-interface.sch" 60
F2 "RXD" O L 1950 2350 60 
F3 "TXD" I L 1950 2450 60 
F4 "STBY" I L 1950 2550 60 
F5 "COM_CAN-" B R 2900 2550 60 
F6 "COM_CAN+" B R 2900 2650 60 
$EndSheet
$Sheet
S 1950 3050 950  500 
U 5709853A
F0 "CAN 4" 60
F1 "can-interface.sch" 60
F2 "RXD" O L 1950 3150 60 
F3 "TXD" I L 1950 3250 60 
F4 "STBY" I L 1950 3350 60 
F5 "COM_CAN-" B R 2900 3350 60 
F6 "COM_CAN+" B R 2900 3450 60 
$EndSheet
Wire Wire Line
	1400 3250 1950 3250
Wire Wire Line
	1400 3150 1950 3150
Wire Wire Line
	1400 2450 1950 2450
Wire Wire Line
	1400 2350 1950 2350
Wire Wire Line
	1400 1650 1950 1650
Wire Wire Line
	1400 1550 1950 1550
Wire Wire Line
	1400 850  1950 850 
Wire Wire Line
	1400 750  1950 750 
Wire Wire Line
	1800 3850 1200 3850
Connection ~ 1800 1750
Wire Wire Line
	1950 1750 1800 1750
Connection ~ 1800 2550
Wire Wire Line
	1800 2550 1950 2550
Connection ~ 1800 3350
Wire Wire Line
	1800 3350 1950 3350
Wire Wire Line
	1800 950  1800 3850
Wire Wire Line
	1950 950  1800 950 
Wire Wire Line
	2900 1750 3350 1750
Entry Wire Line
	3350 1750 3450 1850
Wire Wire Line
	2900 1850 3350 1850
Wire Wire Line
	3350 2550 2900 2550
Wire Wire Line
	3350 2650 2900 2650
Wire Wire Line
	3350 3350 2900 3350
Wire Wire Line
	3350 3450 2900 3450
Entry Wire Line
	3350 1850 3450 1950
Entry Wire Line
	3350 2550 3450 2650
Entry Wire Line
	3350 2650 3450 2750
Entry Wire Line
	3350 3350 3450 3450
Entry Wire Line
	3350 3450 3450 3550
Wire Bus Line
	3450 1050 3450 3550
Text Label 3350 1750 2    60   ~ 0
COM_LO
Text Label 3350 1850 2    60   ~ 0
COM_HI
Text Label 3350 2550 2    60   ~ 0
COM_LO
Text Label 3350 2650 2    60   ~ 0
COM_HI
Text Label 3350 3350 2    60   ~ 0
COM_LO
Text Label 3350 3450 2    60   ~ 0
COM_HI
Wire Wire Line
	2900 950  3350 950 
Wire Wire Line
	2900 1050 3350 1050
Entry Wire Line
	3350 950  3450 1050
Entry Wire Line
	3350 1050 3450 1150
Text Label 3350 950  2    60   ~ 0
COM_LO
Text Label 3350 1050 2    60   ~ 0
COM_HI
$Comp
L R R9
U 1 1 570A351E
P 5200 1050
F 0 "R9" V 5280 1050 50  0000 C CNN
F 1 "120R 250mW" V 5100 1050 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5130 1050 50  0001 C CNN
F 3 "" H 5200 1050 50  0000 C CNN
	1    5200 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 1050 4750 1050
Wire Wire Line
	5350 1050 5700 1050
Text Label 5700 1050 2    60   ~ 0
COM_HI
Text Label 4300 1050 0    60   ~ 0
COM_LO
Text Notes 4250 850  0    60   ~ 0
Common CANbus termination
$Comp
L R R10
U 1 1 5707BAE6
P 5200 1300
F 0 "R10" V 5100 1300 50  0000 C CNN
F 1 "120R 250mW" V 5300 1300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5130 1300 50  0001 C CNN
F 3 "" H 5200 1300 50  0000 C CNN
	1    5200 1300
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 1300 4750 1300
Wire Wire Line
	5350 1300 5700 1300
Text Label 5700 1300 2    60   ~ 0
COM_HI
Text Label 4300 1300 0    60   ~ 0
COM_LO
$Comp
L Jumper_NC_Small JP3
U 1 1 570CC4EE
P 4850 1300
F 0 "JP3" H 4850 1380 50  0000 C CNN
F 1 "Jumper_NC_Small" H 4860 1240 50  0001 C CNN
F 2 "footprints:Pin_Header_Straight_1x02" H 4850 1300 50  0001 C CNN
F 3 "" H 4850 1300 50  0000 C CNN
	1    4850 1300
	-1   0    0    -1  
$EndComp
$Comp
L Jumper_NC_Small JP2
U 1 1 570CC765
P 4850 1050
F 0 "JP2" H 4850 1130 50  0000 C CNN
F 1 "Jumper_NC_Small" H 4860 990 50  0001 C CNN
F 2 "footprints:Pin_Header_Straight_1x02" H 4850 1050 50  0001 C CNN
F 3 "" H 4850 1050 50  0000 C CNN
	1    4850 1050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4950 1050 5050 1050
Wire Wire Line
	5050 1300 4950 1300
$EndSCHEMATC
