EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:microchip_dspic33dsc
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:Altera
LIBS:analog_devices
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:ir
LIBS:Lattice
LIBS:logo
LIBS:maxim
LIBS:motor_drivers
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:Xicor
LIBS:Zilog
LIBS:MCP2562FD
LIBS:vccio
LIBS:mcp
LIBS:JX1
LIBS:JX2
LIBS:gates
LIBS:usblc6
LIBS:canbench-hw-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 10
Title "CANbus MicroZed CarrierCard"
Date "25.4.2016"
Rev "RevA"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 3050 700  0    118  ~ 0
User LEDs
$Comp
L LED D7
U 1 1 56FF46EC
P 9400 3150
F 0 "D7" H 9500 3050 50  0000 C CNN
F 1 "LED_RED" H 9200 3050 50  0000 C CNN
F 2 "LEDs:LED_0805" H 9400 3150 50  0001 C CNN
F 3 "" H 9400 3150 50  0000 C CNN
	1    9400 3150
	-1   0    0    -1  
$EndComp
$Comp
L R R23
U 1 1 56FF46FF
P 8950 3150
F 0 "R23" V 9030 3150 50  0000 C CNN
F 1 "560R" V 8950 3150 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8880 3150 50  0001 C CNN
F 3 "" H 8950 3150 50  0000 C CNN
	1    8950 3150
	0    1    -1   0   
$EndComp
$Comp
L LED D8
U 1 1 56FF527E
P 9400 2950
F 0 "D8" H 9500 2850 50  0000 C CNN
F 1 "LED_RED" H 9200 2850 50  0000 C CNN
F 2 "LEDs:LED_0805" H 9400 2950 50  0001 C CNN
F 3 "" H 9400 2950 50  0000 C CNN
	1    9400 2950
	-1   0    0    -1  
$EndComp
$Comp
L R R24
U 1 1 56FF5291
P 8950 2950
F 0 "R24" V 9030 2950 50  0000 C CNN
F 1 "560R" V 8950 2950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8880 2950 50  0001 C CNN
F 3 "" H 8950 2950 50  0000 C CNN
	1    8950 2950
	0    1    -1   0   
$EndComp
$Comp
L LED D9
U 1 1 56FF5875
P 9400 2750
F 0 "D9" H 9500 2650 50  0000 C CNN
F 1 "LED_RED" H 9200 2650 50  0000 C CNN
F 2 "LEDs:LED_0805" H 9400 2750 50  0001 C CNN
F 3 "" H 9400 2750 50  0000 C CNN
	1    9400 2750
	-1   0    0    -1  
$EndComp
$Comp
L R R25
U 1 1 56FF5888
P 8950 2750
F 0 "R25" V 9030 2750 50  0000 C CNN
F 1 "560R" V 8950 2750 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8880 2750 50  0001 C CNN
F 3 "" H 8950 2750 50  0000 C CNN
	1    8950 2750
	0    1    -1   0   
$EndComp
$Comp
L LED D10
U 1 1 56FF589A
P 9400 2550
F 0 "D10" H 9500 2450 50  0000 C CNN
F 1 "LED_RED" H 9200 2450 50  0000 C CNN
F 2 "LEDs:LED_0805" H 9400 2550 50  0001 C CNN
F 3 "" H 9400 2550 50  0000 C CNN
	1    9400 2550
	-1   0    0    -1  
$EndComp
$Comp
L R R26
U 1 1 56FF58AD
P 8950 2550
F 0 "R26" V 9030 2550 50  0000 C CNN
F 1 "560R" V 8950 2550 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8880 2550 50  0001 C CNN
F 3 "" H 8950 2550 50  0000 C CNN
	1    8950 2550
	0    1    -1   0   
$EndComp
Text HLabel 8500 850  0    60   Input ~ 0
VCCIO
Text Label 8850 850  2    60   ~ 0
VCCIO
Text Label 1250 1000 0    60   ~ 0
VCCIO
$Comp
L GND #PWR0126
U 1 1 57006858
P 1650 1850
F 0 "#PWR0126" H 1650 1600 50  0001 C CNN
F 1 "GND" H 1650 1700 50  0000 C CNN
F 2 "" H 1650 1850 50  0000 C CNN
F 3 "" H 1650 1850 50  0000 C CNN
	1    1650 1850
	1    0    0    -1  
$EndComp
$Comp
L R R32
U 1 1 57006798
P 1650 1250
F 0 "R32" V 1730 1250 50  0000 C CNN
F 1 "3K3" V 1650 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1580 1250 50  0001 C CNN
F 3 "" H 1650 1250 50  0000 C CNN
	1    1650 1250
	1    0    0    -1  
$EndComp
$Comp
L R R31
U 1 1 570065DE
P 1450 1450
F 0 "R31" V 1530 1450 50  0000 C CNN
F 1 "100R" V 1450 1450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1380 1450 50  0001 C CNN
F 3 "" H 1450 1450 50  0000 C CNN
	1    1450 1450
	0    1    1    0   
$EndComp
$Comp
L C C33
U 1 1 57006559
P 1650 1650
F 0 "C33" H 1675 1750 50  0000 L CNN
F 1 "10n" H 1675 1550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1688 1500 50  0001 C CNN
F 3 "" H 1650 1650 50  0000 C CNN
	1    1650 1650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0127
U 1 1 570064B0
P 650 1850
F 0 "#PWR0127" H 650 1600 50  0001 C CNN
F 1 "GND" H 650 1700 50  0000 C CNN
F 2 "" H 650 1850 50  0000 C CNN
F 3 "" H 650 1850 50  0000 C CNN
	1    650  1850
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW4
U 1 1 5700643E
P 950 1450
F 0 "SW4" H 1100 1560 50  0000 C CNN
F 1 "SW_PUSH" H 950 1370 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_EVQP0" H 950 1450 50  0001 C CNN
F 3 "" H 950 1450 50  0000 C CNN
	1    950  1450
	1    0    0    -1  
$EndComp
$Comp
L SW_DIP_x8 S1
U 1 1 5702E44E
P 1000 2950
F 0 "S1" H 1000 2500 50  0000 C CNN
F 1 "SW_DIP_x8" H 1000 3400 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_DIP_x8_Slide" H 950 2950 50  0001 C CNN
F 3 "" H 950 2950 50  0000 C CNN
	1    1000 2950
	1    0    0    -1  
$EndComp
$Comp
L R R43
U 1 1 5702E9D6
P 1550 3300
F 0 "R43" V 1600 3500 50  0000 C CNN
F 1 "100R" V 1550 3300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1480 3300 50  0001 C CNN
F 3 "" H 1550 3300 50  0000 C CNN
	1    1550 3300
	0    1    -1   0   
$EndComp
Text Label 2400 3300 2    60   ~ 0
SW1
$Comp
L R R39
U 1 1 57035424
P 3350 2850
F 0 "R39" V 3430 2850 50  0000 C CNN
F 1 "3K3" V 3350 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3280 2850 50  0001 C CNN
F 3 "" H 3350 2850 50  0000 C CNN
	1    3350 2850
	1    0    0    -1  
$EndComp
$Comp
L C C37
U 1 1 57035540
P 3350 3250
F 0 "C37" H 3375 3350 50  0000 L CNN
F 1 "10n" H 3375 3150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3388 3100 50  0001 C CNN
F 3 "" H 3350 3250 50  0000 C CNN
	1    3350 3250
	1    0    0    -1  
$EndComp
Text Label 3050 3050 0    60   ~ 0
SW1
$Comp
L GND #PWR0128
U 1 1 57035895
P 3350 3500
F 0 "#PWR0128" H 3350 3250 50  0001 C CNN
F 1 "GND" H 3350 3350 50  0000 C CNN
F 2 "" H 3350 3500 50  0000 C CNN
F 3 "" H 3350 3500 50  0000 C CNN
	1    3350 3500
	1    0    0    -1  
$EndComp
Text Label 3050 2600 0    60   ~ 0
VCCIO
$Comp
L R R40
U 1 1 57035AC0
P 3800 2850
F 0 "R40" V 3880 2850 50  0000 C CNN
F 1 "3K3" V 3800 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3730 2850 50  0001 C CNN
F 3 "" H 3800 2850 50  0000 C CNN
	1    3800 2850
	1    0    0    -1  
$EndComp
$Comp
L C C38
U 1 1 57035AC6
P 3800 3250
F 0 "C38" H 3825 3350 50  0000 L CNN
F 1 "10n" H 3825 3150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3838 3100 50  0001 C CNN
F 3 "" H 3800 3250 50  0000 C CNN
	1    3800 3250
	1    0    0    -1  
$EndComp
Text Label 3500 3050 0    60   ~ 0
SW2
$Comp
L GND #PWR0129
U 1 1 57035AD0
P 3800 3500
F 0 "#PWR0129" H 3800 3250 50  0001 C CNN
F 1 "GND" H 3800 3350 50  0000 C CNN
F 2 "" H 3800 3500 50  0000 C CNN
F 3 "" H 3800 3500 50  0000 C CNN
	1    3800 3500
	1    0    0    -1  
$EndComp
$Comp
L R R41
U 1 1 57035B36
P 4250 2850
F 0 "R41" V 4330 2850 50  0000 C CNN
F 1 "3K3" V 4250 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4180 2850 50  0001 C CNN
F 3 "" H 4250 2850 50  0000 C CNN
	1    4250 2850
	1    0    0    -1  
$EndComp
$Comp
L C C39
U 1 1 57035B3C
P 4250 3250
F 0 "C39" H 4275 3350 50  0000 L CNN
F 1 "10n" H 4275 3150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4288 3100 50  0001 C CNN
F 3 "" H 4250 3250 50  0000 C CNN
	1    4250 3250
	1    0    0    -1  
$EndComp
Text Label 3950 3050 0    60   ~ 0
SW3
$Comp
L GND #PWR0130
U 1 1 57035B46
P 4250 3500
F 0 "#PWR0130" H 4250 3250 50  0001 C CNN
F 1 "GND" H 4250 3350 50  0000 C CNN
F 2 "" H 4250 3500 50  0000 C CNN
F 3 "" H 4250 3500 50  0000 C CNN
	1    4250 3500
	1    0    0    -1  
$EndComp
$Comp
L R R42
U 1 1 57035C42
P 4700 2850
F 0 "R42" V 4780 2850 50  0000 C CNN
F 1 "3K3" V 4700 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4630 2850 50  0001 C CNN
F 3 "" H 4700 2850 50  0000 C CNN
	1    4700 2850
	1    0    0    -1  
$EndComp
$Comp
L C C40
U 1 1 57035C48
P 4700 3250
F 0 "C40" H 4725 3350 50  0000 L CNN
F 1 "10n" H 4725 3150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4738 3100 50  0001 C CNN
F 3 "" H 4700 3250 50  0000 C CNN
	1    4700 3250
	1    0    0    -1  
$EndComp
Text Label 4400 3050 0    60   ~ 0
SW4
$Comp
L GND #PWR0131
U 1 1 57035C52
P 4700 3500
F 0 "#PWR0131" H 4700 3250 50  0001 C CNN
F 1 "GND" H 4700 3350 50  0000 C CNN
F 2 "" H 4700 3500 50  0000 C CNN
F 3 "" H 4700 3500 50  0000 C CNN
	1    4700 3500
	1    0    0    -1  
$EndComp
$Comp
L R R51
U 1 1 57039124
P 5150 2850
F 0 "R51" V 5230 2850 50  0000 C CNN
F 1 "3K3" V 5150 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5080 2850 50  0001 C CNN
F 3 "" H 5150 2850 50  0000 C CNN
	1    5150 2850
	1    0    0    -1  
$EndComp
$Comp
L C C41
U 1 1 5703912A
P 5150 3250
F 0 "C41" H 5175 3350 50  0000 L CNN
F 1 "10n" H 5175 3150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5188 3100 50  0001 C CNN
F 3 "" H 5150 3250 50  0000 C CNN
	1    5150 3250
	1    0    0    -1  
$EndComp
Text Label 4850 3050 0    60   ~ 0
SW5
$Comp
L GND #PWR0132
U 1 1 57039134
P 5150 3500
F 0 "#PWR0132" H 5150 3250 50  0001 C CNN
F 1 "GND" H 5150 3350 50  0000 C CNN
F 2 "" H 5150 3500 50  0000 C CNN
F 3 "" H 5150 3500 50  0000 C CNN
	1    5150 3500
	1    0    0    -1  
$EndComp
$Comp
L R R52
U 1 1 5703913B
P 5600 2850
F 0 "R52" V 5680 2850 50  0000 C CNN
F 1 "3K3" V 5600 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5530 2850 50  0001 C CNN
F 3 "" H 5600 2850 50  0000 C CNN
	1    5600 2850
	1    0    0    -1  
$EndComp
$Comp
L C C42
U 1 1 57039141
P 5600 3250
F 0 "C42" H 5625 3350 50  0000 L CNN
F 1 "10n" H 5625 3150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5638 3100 50  0001 C CNN
F 3 "" H 5600 3250 50  0000 C CNN
	1    5600 3250
	1    0    0    -1  
$EndComp
Text Label 5300 3050 0    60   ~ 0
SW6
$Comp
L GND #PWR0133
U 1 1 5703914B
P 5600 3500
F 0 "#PWR0133" H 5600 3250 50  0001 C CNN
F 1 "GND" H 5600 3350 50  0000 C CNN
F 2 "" H 5600 3500 50  0000 C CNN
F 3 "" H 5600 3500 50  0000 C CNN
	1    5600 3500
	1    0    0    -1  
$EndComp
$Comp
L R R53
U 1 1 57039152
P 6050 2850
F 0 "R53" V 6130 2850 50  0000 C CNN
F 1 "3K3" V 6050 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5980 2850 50  0001 C CNN
F 3 "" H 6050 2850 50  0000 C CNN
	1    6050 2850
	1    0    0    -1  
$EndComp
$Comp
L C C43
U 1 1 57039158
P 6050 3250
F 0 "C43" H 6075 3350 50  0000 L CNN
F 1 "10n" H 6075 3150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6088 3100 50  0001 C CNN
F 3 "" H 6050 3250 50  0000 C CNN
	1    6050 3250
	1    0    0    -1  
$EndComp
Text Label 5750 3050 0    60   ~ 0
SW7
$Comp
L GND #PWR0134
U 1 1 57039162
P 6050 3500
F 0 "#PWR0134" H 6050 3250 50  0001 C CNN
F 1 "GND" H 6050 3350 50  0000 C CNN
F 2 "" H 6050 3500 50  0000 C CNN
F 3 "" H 6050 3500 50  0000 C CNN
	1    6050 3500
	1    0    0    -1  
$EndComp
$Comp
L R R54
U 1 1 57039169
P 6500 2850
F 0 "R54" V 6580 2850 50  0000 C CNN
F 1 "3K3" V 6500 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6430 2850 50  0001 C CNN
F 3 "" H 6500 2850 50  0000 C CNN
	1    6500 2850
	1    0    0    -1  
$EndComp
$Comp
L C C44
U 1 1 5703916F
P 6500 3250
F 0 "C44" H 6525 3350 50  0000 L CNN
F 1 "10n" H 6525 3150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6538 3100 50  0001 C CNN
F 3 "" H 6500 3250 50  0000 C CNN
	1    6500 3250
	1    0    0    -1  
$EndComp
Text Label 6200 3050 0    60   ~ 0
SW8
$Comp
L GND #PWR0135
U 1 1 57039179
P 6500 3500
F 0 "#PWR0135" H 6500 3250 50  0001 C CNN
F 1 "GND" H 6500 3350 50  0000 C CNN
F 2 "" H 6500 3500 50  0000 C CNN
F 3 "" H 6500 3500 50  0000 C CNN
	1    6500 3500
	1    0    0    -1  
$EndComp
$Comp
L R R44
U 1 1 5703A492
P 1550 3200
F 0 "R44" V 1600 3400 50  0000 C CNN
F 1 "100R" V 1550 3200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1480 3200 50  0001 C CNN
F 3 "" H 1550 3200 50  0000 C CNN
	1    1550 3200
	0    1    -1   0   
$EndComp
Text Label 2400 3200 2    60   ~ 0
SW2
$Comp
L R R45
U 1 1 5703A724
P 1550 3100
F 0 "R45" V 1600 3300 50  0000 C CNN
F 1 "100R" V 1550 3100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1480 3100 50  0001 C CNN
F 3 "" H 1550 3100 50  0000 C CNN
	1    1550 3100
	0    1    -1   0   
$EndComp
Text Label 2400 3100 2    60   ~ 0
SW3
$Comp
L R R46
U 1 1 5703A72D
P 1550 3000
F 0 "R46" V 1600 3200 50  0000 C CNN
F 1 "100R" V 1550 3000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1480 3000 50  0001 C CNN
F 3 "" H 1550 3000 50  0000 C CNN
	1    1550 3000
	0    1    -1   0   
$EndComp
Text Label 2400 3000 2    60   ~ 0
SW4
$Comp
L R R47
U 1 1 5703A7ED
P 1550 2900
F 0 "R47" V 1600 3100 50  0000 C CNN
F 1 "100R" V 1550 2900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1480 2900 50  0001 C CNN
F 3 "" H 1550 2900 50  0000 C CNN
	1    1550 2900
	0    1    -1   0   
$EndComp
Text Label 2400 2900 2    60   ~ 0
SW5
$Comp
L R R48
U 1 1 5703A7F6
P 1550 2800
F 0 "R48" V 1600 3000 50  0000 C CNN
F 1 "100R" V 1550 2800 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1480 2800 50  0001 C CNN
F 3 "" H 1550 2800 50  0000 C CNN
	1    1550 2800
	0    1    -1   0   
$EndComp
Text Label 2400 2800 2    60   ~ 0
SW6
$Comp
L R R49
U 1 1 5703A7FF
P 1550 2700
F 0 "R49" V 1600 2900 50  0000 C CNN
F 1 "100R" V 1550 2700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1480 2700 50  0001 C CNN
F 3 "" H 1550 2700 50  0000 C CNN
	1    1550 2700
	0    1    -1   0   
$EndComp
Text Label 2400 2700 2    60   ~ 0
SW7
$Comp
L R R50
U 1 1 5703A808
P 1550 2600
F 0 "R50" V 1600 2800 50  0000 C CNN
F 1 "100R" V 1550 2600 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1480 2600 50  0001 C CNN
F 3 "" H 1550 2600 50  0000 C CNN
	1    1550 2600
	0    1    -1   0   
$EndComp
Text Label 2400 2600 2    60   ~ 0
SW8
$Comp
L GND #PWR0136
U 1 1 5703E0FE
P 600 3500
F 0 "#PWR0136" H 600 3250 50  0001 C CNN
F 1 "GND" H 600 3350 50  0000 C CNN
F 2 "" H 600 3500 50  0000 C CNN
F 3 "" H 600 3500 50  0000 C CNN
	1    600  3500
	1    0    0    -1  
$EndComp
Text HLabel 2400 3300 2    60   Input ~ 0
SW1
Text HLabel 2400 3200 2    60   Input ~ 0
SW2
Text HLabel 2400 3100 2    60   Input ~ 0
SW3
Text HLabel 2400 3000 2    60   Input ~ 0
SW4
Text HLabel 2400 2900 2    60   Input ~ 0
SW5
Text HLabel 2400 2800 2    60   Input ~ 0
SW6
Text HLabel 2400 2700 2    60   Input ~ 0
SW7
Text HLabel 2400 2600 2    60   Input ~ 0
SW8
Text HLabel 1800 1450 2    60   Input ~ 0
KEY1
Text Label 2850 1000 0    60   ~ 0
VCCIO
$Comp
L GND #PWR0137
U 1 1 57061E63
P 3250 1850
F 0 "#PWR0137" H 3250 1600 50  0001 C CNN
F 1 "GND" H 3250 1700 50  0000 C CNN
F 2 "" H 3250 1850 50  0000 C CNN
F 3 "" H 3250 1850 50  0000 C CNN
	1    3250 1850
	1    0    0    -1  
$EndComp
$Comp
L R R34
U 1 1 57061E69
P 3250 1250
F 0 "R34" V 3330 1250 50  0000 C CNN
F 1 "3K3" V 3250 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3180 1250 50  0001 C CNN
F 3 "" H 3250 1250 50  0000 C CNN
	1    3250 1250
	1    0    0    -1  
$EndComp
$Comp
L R R33
U 1 1 57061E70
P 3050 1450
F 0 "R33" V 3130 1450 50  0000 C CNN
F 1 "100R" V 3050 1450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2980 1450 50  0001 C CNN
F 3 "" H 3050 1450 50  0000 C CNN
	1    3050 1450
	0    1    1    0   
$EndComp
$Comp
L C C34
U 1 1 57061E76
P 3250 1650
F 0 "C34" H 3275 1750 50  0000 L CNN
F 1 "10n" H 3275 1550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3288 1500 50  0001 C CNN
F 3 "" H 3250 1650 50  0000 C CNN
	1    3250 1650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0138
U 1 1 57061E7D
P 2250 1850
F 0 "#PWR0138" H 2250 1600 50  0001 C CNN
F 1 "GND" H 2250 1700 50  0000 C CNN
F 2 "" H 2250 1850 50  0000 C CNN
F 3 "" H 2250 1850 50  0000 C CNN
	1    2250 1850
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW5
U 1 1 57061E83
P 2550 1450
F 0 "SW5" H 2700 1560 50  0000 C CNN
F 1 "SW_PUSH" H 2550 1370 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_EVQP0" H 2550 1450 50  0001 C CNN
F 3 "" H 2550 1450 50  0000 C CNN
	1    2550 1450
	1    0    0    -1  
$EndComp
Text HLabel 3400 1450 2    60   Input ~ 0
KEY2
Text Label 4500 1000 0    60   ~ 0
VCCIO
$Comp
L GND #PWR0139
U 1 1 57063072
P 4900 1850
F 0 "#PWR0139" H 4900 1600 50  0001 C CNN
F 1 "GND" H 4900 1700 50  0000 C CNN
F 2 "" H 4900 1850 50  0000 C CNN
F 3 "" H 4900 1850 50  0000 C CNN
	1    4900 1850
	1    0    0    -1  
$EndComp
$Comp
L R R36
U 1 1 57063078
P 4900 1250
F 0 "R36" V 4980 1250 50  0000 C CNN
F 1 "3K3" V 4900 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4830 1250 50  0001 C CNN
F 3 "" H 4900 1250 50  0000 C CNN
	1    4900 1250
	1    0    0    -1  
$EndComp
$Comp
L R R35
U 1 1 5706307F
P 4700 1450
F 0 "R35" V 4780 1450 50  0000 C CNN
F 1 "100R" V 4700 1450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4630 1450 50  0001 C CNN
F 3 "" H 4700 1450 50  0000 C CNN
	1    4700 1450
	0    1    1    0   
$EndComp
$Comp
L C C35
U 1 1 57063085
P 4900 1650
F 0 "C35" H 4925 1750 50  0000 L CNN
F 1 "10n" H 4925 1550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4938 1500 50  0001 C CNN
F 3 "" H 4900 1650 50  0000 C CNN
	1    4900 1650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0140
U 1 1 5706308C
P 3900 1850
F 0 "#PWR0140" H 3900 1600 50  0001 C CNN
F 1 "GND" H 3900 1700 50  0000 C CNN
F 2 "" H 3900 1850 50  0000 C CNN
F 3 "" H 3900 1850 50  0000 C CNN
	1    3900 1850
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW6
U 1 1 57063092
P 4200 1450
F 0 "SW6" H 4350 1560 50  0000 C CNN
F 1 "SW_PUSH" H 4200 1370 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_EVQP0" H 4200 1450 50  0001 C CNN
F 3 "" H 4200 1450 50  0000 C CNN
	1    4200 1450
	1    0    0    -1  
$EndComp
Text HLabel 5050 1450 2    60   Input ~ 0
KEY3
Text Label 6100 1000 0    60   ~ 0
VCCIO
$Comp
L GND #PWR0141
U 1 1 570630A2
P 6500 1850
F 0 "#PWR0141" H 6500 1600 50  0001 C CNN
F 1 "GND" H 6500 1700 50  0000 C CNN
F 2 "" H 6500 1850 50  0000 C CNN
F 3 "" H 6500 1850 50  0000 C CNN
	1    6500 1850
	1    0    0    -1  
$EndComp
$Comp
L R R38
U 1 1 570630A8
P 6500 1250
F 0 "R38" V 6580 1250 50  0000 C CNN
F 1 "3K3" V 6500 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6430 1250 50  0001 C CNN
F 3 "" H 6500 1250 50  0000 C CNN
	1    6500 1250
	1    0    0    -1  
$EndComp
$Comp
L R R37
U 1 1 570630AF
P 6300 1450
F 0 "R37" V 6380 1450 50  0000 C CNN
F 1 "100R" V 6300 1450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6230 1450 50  0001 C CNN
F 3 "" H 6300 1450 50  0000 C CNN
	1    6300 1450
	0    1    1    0   
$EndComp
$Comp
L C C36
U 1 1 570630B5
P 6500 1650
F 0 "C36" H 6525 1750 50  0000 L CNN
F 1 "10n" H 6525 1550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6538 1500 50  0001 C CNN
F 3 "" H 6500 1650 50  0000 C CNN
	1    6500 1650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0142
U 1 1 570630BC
P 5500 1850
F 0 "#PWR0142" H 5500 1600 50  0001 C CNN
F 1 "GND" H 5500 1700 50  0000 C CNN
F 2 "" H 5500 1850 50  0000 C CNN
F 3 "" H 5500 1850 50  0000 C CNN
	1    5500 1850
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW7
U 1 1 570630C2
P 5800 1450
F 0 "SW7" H 5950 1560 50  0000 C CNN
F 1 "SW_PUSH" H 5800 1370 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_EVQP0" H 5800 1450 50  0001 C CNN
F 3 "" H 5800 1450 50  0000 C CNN
	1    5800 1450
	1    0    0    -1  
$EndComp
Text HLabel 6650 1450 2    60   Input ~ 0
KEY4
Wire Wire Line
	8500 850  8850 850 
Connection ~ 1650 1450
Wire Wire Line
	1650 1400 1650 1500
Wire Wire Line
	1600 1450 1800 1450
Wire Wire Line
	1650 1000 1250 1000
Wire Wire Line
	1650 1100 1650 1000
Wire Wire Line
	1650 1850 1650 1800
Wire Wire Line
	1250 1450 1300 1450
Wire Wire Line
	650  1450 650  1850
Wire Wire Line
	1300 3300 1400 3300
Wire Wire Line
	1700 3300 2400 3300
Wire Wire Line
	3350 3000 3350 3100
Wire Wire Line
	3350 3050 3050 3050
Connection ~ 3350 3050
Wire Wire Line
	3350 3400 3350 3500
Wire Wire Line
	3350 2700 3350 2600
Wire Wire Line
	3050 2600 6500 2600
Wire Wire Line
	3800 3000 3800 3100
Wire Wire Line
	3800 3050 3500 3050
Connection ~ 3800 3050
Wire Wire Line
	3800 3400 3800 3500
Wire Wire Line
	3800 2600 3800 2700
Wire Wire Line
	4250 3000 4250 3100
Wire Wire Line
	4250 3050 3950 3050
Connection ~ 4250 3050
Wire Wire Line
	4250 3400 4250 3500
Wire Wire Line
	4250 2600 4250 2700
Wire Wire Line
	4700 3000 4700 3100
Wire Wire Line
	4700 3050 4400 3050
Connection ~ 4700 3050
Wire Wire Line
	4700 3400 4700 3500
Wire Wire Line
	4700 2600 4700 2700
Connection ~ 3350 2600
Connection ~ 3800 2600
Connection ~ 4250 2600
Wire Wire Line
	5150 3000 5150 3100
Wire Wire Line
	5150 3050 4850 3050
Connection ~ 5150 3050
Wire Wire Line
	5150 3400 5150 3500
Wire Wire Line
	5600 3000 5600 3100
Wire Wire Line
	5600 3050 5300 3050
Connection ~ 5600 3050
Wire Wire Line
	5600 3400 5600 3500
Wire Wire Line
	6050 3000 6050 3100
Wire Wire Line
	6050 3050 5750 3050
Connection ~ 6050 3050
Wire Wire Line
	6050 3400 6050 3500
Wire Wire Line
	6500 3000 6500 3100
Wire Wire Line
	6500 3050 6200 3050
Connection ~ 6500 3050
Wire Wire Line
	6500 3400 6500 3500
Wire Wire Line
	5150 2600 5150 2700
Connection ~ 4700 2600
Wire Wire Line
	5600 2600 5600 2700
Connection ~ 5150 2600
Wire Wire Line
	6050 2600 6050 2700
Connection ~ 5600 2600
Wire Wire Line
	6500 2600 6500 2700
Connection ~ 6050 2600
Wire Wire Line
	1300 3200 1400 3200
Wire Wire Line
	1700 3200 2400 3200
Wire Wire Line
	1300 3100 1400 3100
Wire Wire Line
	1700 3100 2400 3100
Wire Wire Line
	1300 3000 1400 3000
Wire Wire Line
	1700 3000 2400 3000
Wire Wire Line
	1300 2900 1400 2900
Wire Wire Line
	1700 2900 2400 2900
Wire Wire Line
	1300 2800 1400 2800
Wire Wire Line
	1700 2800 2400 2800
Wire Wire Line
	1300 2700 1400 2700
Wire Wire Line
	1700 2700 2400 2700
Wire Wire Line
	1300 2600 1400 2600
Wire Wire Line
	1700 2600 2400 2600
Wire Wire Line
	700  2600 600  2600
Wire Wire Line
	600  2600 600  3500
Wire Wire Line
	700  2700 600  2700
Connection ~ 600  2700
Wire Wire Line
	700  2800 600  2800
Connection ~ 600  2800
Wire Wire Line
	700  2900 600  2900
Connection ~ 600  2900
Wire Wire Line
	600  3000 700  3000
Connection ~ 600  3000
Wire Wire Line
	700  3100 600  3100
Connection ~ 600  3100
Wire Wire Line
	700  3200 600  3200
Connection ~ 600  3200
Wire Wire Line
	700  3300 600  3300
Connection ~ 600  3300
Connection ~ 3250 1450
Wire Wire Line
	3250 1400 3250 1500
Wire Wire Line
	3200 1450 3400 1450
Wire Wire Line
	3250 1000 2850 1000
Wire Wire Line
	3250 1100 3250 1000
Wire Wire Line
	3250 1850 3250 1800
Wire Wire Line
	2850 1450 2900 1450
Wire Wire Line
	2250 1450 2250 1850
Connection ~ 4900 1450
Wire Wire Line
	4900 1400 4900 1500
Wire Wire Line
	4850 1450 5050 1450
Wire Wire Line
	4900 1000 4500 1000
Wire Wire Line
	4900 1100 4900 1000
Wire Wire Line
	4900 1850 4900 1800
Wire Wire Line
	4500 1450 4550 1450
Wire Wire Line
	3900 1450 3900 1850
Connection ~ 6500 1450
Wire Wire Line
	6500 1400 6500 1500
Wire Wire Line
	6450 1450 6650 1450
Wire Wire Line
	6500 1000 6100 1000
Wire Wire Line
	6500 1100 6500 1000
Wire Wire Line
	6500 1850 6500 1800
Wire Wire Line
	6100 1450 6150 1450
Wire Wire Line
	5500 1450 5500 1850
Wire Wire Line
	7700 3150 7950 3150
Wire Wire Line
	7700 2950 7950 2950
Wire Wire Line
	7700 2750 7950 2750
Wire Wire Line
	7700 2550 7950 2550
Text HLabel 7700 3150 0    60   Input ~ 0
LED1
Text HLabel 7700 2950 0    60   Input ~ 0
LED2
Text HLabel 7700 2750 0    60   Input ~ 0
LED3
Text HLabel 7700 2550 0    60   Input ~ 0
LED4
Wire Wire Line
	9100 3150 9200 3150
Wire Wire Line
	9200 2950 9100 2950
Wire Wire Line
	9100 2750 9200 2750
Wire Wire Line
	9200 2550 9100 2550
$Comp
L LED D11
U 1 1 570E8238
P 9400 2350
F 0 "D11" H 9500 2250 50  0000 C CNN
F 1 "LED_RED" H 9200 2250 50  0000 C CNN
F 2 "LEDs:LED_0805" H 9400 2350 50  0001 C CNN
F 3 "" H 9400 2350 50  0000 C CNN
	1    9400 2350
	-1   0    0    -1  
$EndComp
$Comp
L R R27
U 1 1 570E823E
P 8950 2350
F 0 "R27" V 9030 2350 50  0000 C CNN
F 1 "560R" V 8950 2350 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8880 2350 50  0001 C CNN
F 3 "" H 8950 2350 50  0000 C CNN
	1    8950 2350
	0    1    -1   0   
$EndComp
$Comp
L LED D12
U 1 1 570E8244
P 9400 2150
F 0 "D12" H 9500 2050 50  0000 C CNN
F 1 "LED_RED" H 9200 2050 50  0000 C CNN
F 2 "LEDs:LED_0805" H 9400 2150 50  0001 C CNN
F 3 "" H 9400 2150 50  0000 C CNN
	1    9400 2150
	-1   0    0    -1  
$EndComp
$Comp
L R R28
U 1 1 570E824A
P 8950 2150
F 0 "R28" V 9030 2150 50  0000 C CNN
F 1 "560R" V 8950 2150 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8880 2150 50  0001 C CNN
F 3 "" H 8950 2150 50  0000 C CNN
	1    8950 2150
	0    1    -1   0   
$EndComp
$Comp
L LED D13
U 1 1 570E8250
P 9400 1950
F 0 "D13" H 9500 1850 50  0000 C CNN
F 1 "LED_RED" H 9200 1850 50  0000 C CNN
F 2 "LEDs:LED_0805" H 9400 1950 50  0001 C CNN
F 3 "" H 9400 1950 50  0000 C CNN
	1    9400 1950
	-1   0    0    -1  
$EndComp
$Comp
L R R29
U 1 1 570E8256
P 8950 1950
F 0 "R29" V 9030 1950 50  0000 C CNN
F 1 "560R" V 8950 1950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8880 1950 50  0001 C CNN
F 3 "" H 8950 1950 50  0000 C CNN
	1    8950 1950
	0    1    -1   0   
$EndComp
$Comp
L LED D14
U 1 1 570E825C
P 9400 1750
F 0 "D14" H 9500 1650 50  0000 C CNN
F 1 "LED_RED" H 9200 1650 50  0000 C CNN
F 2 "LEDs:LED_0805" H 9400 1750 50  0001 C CNN
F 3 "" H 9400 1750 50  0000 C CNN
	1    9400 1750
	-1   0    0    -1  
$EndComp
$Comp
L R R30
U 1 1 570E8262
P 8950 1750
F 0 "R30" V 9030 1750 50  0000 C CNN
F 1 "560R" V 8950 1750 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8880 1750 50  0001 C CNN
F 3 "" H 8950 1750 50  0000 C CNN
	1    8950 1750
	0    1    -1   0   
$EndComp
Wire Wire Line
	7700 2350 7950 2350
Wire Wire Line
	7700 2150 7950 2150
Wire Wire Line
	7700 1950 7950 1950
Wire Wire Line
	7700 1750 7950 1750
Text HLabel 7700 2350 0    60   Input ~ 0
LED5
Text HLabel 7700 2150 0    60   Input ~ 0
LED6
Text HLabel 7700 1950 0    60   Input ~ 0
LED7
Text HLabel 7700 1750 0    60   Input ~ 0
LED8
Wire Wire Line
	9100 2350 9200 2350
Wire Wire Line
	9200 2150 9100 2150
Wire Wire Line
	9100 1950 9200 1950
Wire Wire Line
	9200 1750 9100 1750
$Comp
L C C46
U 1 1 5711357B
P 10600 2150
F 0 "C46" H 10625 2250 50  0000 L CNN
F 1 "100n" H 10625 2050 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 10638 2000 50  0001 C CNN
F 3 "" H 10600 2150 50  0000 C CNN
	1    10600 2150
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR0143
U 1 1 57113581
P 10600 1900
F 0 "#PWR0143" H 10600 1750 50  0001 C CNN
F 1 "+5V" H 10600 2040 50  0000 C CNN
F 2 "" H 10600 1900 50  0000 C CNN
F 3 "" H 10600 1900 50  0000 C CNN
	1    10600 1900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0144
U 1 1 57113587
P 10600 2400
F 0 "#PWR0144" H 10600 2150 50  0001 C CNN
F 1 "GND" H 10600 2250 50  0000 C CNN
F 2 "" H 10600 2400 50  0000 C CNN
F 3 "" H 10600 2400 50  0000 C CNN
	1    10600 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 1900 10600 2000
Wire Wire Line
	10600 2300 10600 2400
$Comp
L 74HCT245 U12
U 1 1 570D0A95
P 8350 1800
F 0 "U12" H 8100 250 60  0000 C CNN
F 1 "74HCT245PW" H 8650 250 60  0000 C CNN
F 2 "footprints:TSSOP20" H 8350 1800 60  0001 C CNN
F 3 "http://www.tme.eu/cz/Document/6b9c9e3c1f2cabc2004604fd7fafbc4d/74HC_HCT245.pdf" H 8350 1800 60  0001 C CNN
F 4 "TME, 74HCT245PW" H 8350 1800 60  0001 C CNN "Supplier"
	1    8350 1800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR0145
U 1 1 570D17B2
P 8350 1300
F 0 "#PWR0145" H 8350 1150 50  0001 C CNN
F 1 "+5V" H 8350 1440 50  0000 C CNN
F 2 "" H 8350 1300 50  0000 C CNN
F 3 "" H 8350 1300 50  0000 C CNN
	1    8350 1300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR0146
U 1 1 570D1877
P 8350 3500
F 0 "#PWR0146" H 8350 3250 50  0001 C CNN
F 1 "GND" H 8350 3350 50  0000 C CNN
F 2 "" H 8350 3500 50  0000 C CNN
F 3 "" H 8350 3500 50  0000 C CNN
	1    8350 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 1300 8350 1400
Wire Wire Line
	8350 3500 8350 3400
$Comp
L GND #PWR0147
U 1 1 570D9379
P 9900 3400
F 0 "#PWR0147" H 9900 3150 50  0001 C CNN
F 1 "GND" H 9900 3250 50  0000 C CNN
F 2 "" H 9900 3400 50  0000 C CNN
F 3 "" H 9900 3400 50  0000 C CNN
	1    9900 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 1550 9900 1550
Wire Wire Line
	9900 1550 9900 3400
Wire Wire Line
	9600 3150 9900 3150
Connection ~ 9900 1750
Wire Wire Line
	9600 2950 9900 2950
Connection ~ 9900 1950
Wire Wire Line
	9600 2750 9900 2750
Connection ~ 9900 2150
Wire Wire Line
	9600 2550 9900 2550
Connection ~ 9900 2350
Wire Wire Line
	9600 2350 9900 2350
Connection ~ 9900 2550
Wire Wire Line
	9600 2150 9900 2150
Connection ~ 9900 2750
Wire Wire Line
	9600 1950 9900 1950
Connection ~ 9900 2950
Wire Wire Line
	9600 1750 9900 1750
Connection ~ 9900 3150
Wire Wire Line
	8750 3150 8800 3150
Wire Wire Line
	8750 2950 8800 2950
Wire Wire Line
	8750 2750 8800 2750
Wire Wire Line
	8750 2550 8800 2550
Wire Wire Line
	8750 2350 8800 2350
Wire Wire Line
	8750 2150 8800 2150
Wire Wire Line
	8750 1950 8800 1950
Wire Wire Line
	8750 1750 8800 1750
$Comp
L +5V #PWR0148
U 1 1 570DEED1
P 7850 1300
F 0 "#PWR0148" H 7850 1150 50  0001 C CNN
F 1 "+5V" H 7850 1440 50  0000 C CNN
F 2 "" H 7850 1300 50  0000 C CNN
F 3 "" H 7850 1300 50  0000 C CNN
	1    7850 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 1300 7850 1550
Wire Wire Line
	7850 1550 7950 1550
$EndSCHEMATC
